import sys
from datetime import datetime

from PySide6.QtCore import Slot, Qt
from PySide6.QtWidgets import QWidget, QLabel, QApplication, QPushButton, QDialog, \
    QVBoxLayout, QHBoxLayout

from model.counter_draft import DraftCounter
from model.counters_list import CountersListModel
from view.counters_list import CountersListWidget
from view.current_counter import CurrentCounterWidget
from view.custom_counter import CustomCounterWidget


class AppUI(QWidget):
    finish_dialog = None

    def __init__(self, app: QApplication):
        super().__init__()
        self.draft_counter = DraftCounter()
        self.counters = CountersListModel(self.draft_counter, 'day')

        self.main_layout = QVBoxLayout(self)

        self.welcome_message = QLabel("Welcome in the very simple Time Clock")
        self.welcome_message.setAlignment(Qt.AlignCenter)
        self.main_layout.addWidget(self.welcome_message, 2)

        self.current_counter_widget = CurrentCounterWidget(self.draft_counter)
        self.main_layout.addWidget(self.current_counter_widget, 1)

        self.detailed_view_button = QPushButton("Show details")
        self.main_layout.addWidget(self.detailed_view_button)
        self.detailed_view_button.clicked.connect(self.show_detailed_view)

        self.custom_counter_widget = CustomCounterWidget(self.draft_counter)
        self.main_layout.addWidget(self.custom_counter_widget)

        self.counters_list_widget = CountersListWidget(self.counters)
        self.main_layout.addWidget(self.counters_list_widget, 10)

        self.reset_button = QPushButton("!!! Reset All !!!")
        self.main_layout.addWidget(self.reset_button)
        self.reset_button.clicked.connect(self.action_reset_all)

        self.hide_detailed_view()
        app.aboutToQuit.connect(self.action_say_good_bye)

    def hide_detailed_view(self):
        self.setMinimumWidth(600)
        self.setMinimumHeight(100)
        self.welcome_message.hide()
        self.detailed_view_button.setText("Show details")
        # noinspection PyUnresolvedReferences
        self.detailed_view_button.clicked.disconnect(self.hide_detailed_view)
        self.detailed_view_button.clicked.connect(self.show_detailed_view)
        self.custom_counter_widget.hide()
        self.counters_list_widget.hide()
        self.reset_button.hide()
        self.window().resize(600, 120)

    def show_detailed_view(self):
        self.setMinimumWidth(700)
        self.setMinimumHeight(400)
        self.welcome_message.show()
        self.detailed_view_button.setText("Hide details")
        # noinspection PyUnresolvedReferences
        self.detailed_view_button.clicked.disconnect(self.show_detailed_view)
        self.detailed_view_button.clicked.connect(self.hide_detailed_view)
        self.custom_counter_widget.show()
        self.counters_list_widget.show()
        self.reset_button.show()

    @Slot()
    def action_say_good_bye(self):
        if self.draft_counter.currently_running:
            self.finish_dialog = QDialog()
            self.finish_dialog.setWindowTitle('QUITTER  ?')
            layout = QVBoxLayout(self.finish_dialog)
            layout.addWidget(QLabel(
                'A counter is running since '
                + datetime.fromtimestamp(self.draft_counter.start_at).strftime('%Y-%m-%d %H:%M:%S')
                + '\nDo you want to finish it ?'
            ))
            actions_frame = QWidget()
            actions_layout = QHBoxLayout(actions_frame)
            terminate_button = QPushButton('Terminate')
            dont_terminate_button = QPushButton('No, Thanks')
            actions_layout.addWidget(terminate_button)
            actions_layout.addWidget(dont_terminate_button)
            layout.addWidget(actions_frame)
            terminate_button.clicked.connect(self.action_finish_before_quit)
            dont_terminate_button.clicked.connect(self.action_dont_finish_before_quit)
            self.finish_dialog.exec()

    @Slot()
    def action_finish_before_quit(self):
        self.draft_counter.stop()
        self.finish_dialog.accept()

    @Slot()
    def action_dont_finish_before_quit(self):
        self.finish_dialog.reject()

    @Slot()
    def action_reset_all(self):
        self.draft_counter.cancel()
        self.counters.reset_all()
        return False


if __name__ == "__main__":
    app = QApplication([])

    widget = AppUI(app)
    with open("style.qss", "r") as f:
        _style = f.read()
        app.setStyleSheet(_style)
    widget.setWindowTitle("Time Clock")
    widget.show()

    sys.exit(app.exec())
