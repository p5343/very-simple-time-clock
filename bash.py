import random
from time import time
from math import floor

from model.counter_draft import DraftCounter
from model.counters_list import CountersListModel

if __name__ == '__main__':
    draft_counter = DraftCounter()
    counters = CountersListModel(draft_counter)
    run = True
    while run:
        print("\nCounters :")
        print(counters)
        print("\nCounter is actually " + ("running" if draft_counter.currently_running else "stopped"))
        command = input("\nWhat do you want to do ? [start,stop,cancel,reset,fixtures,exit]\n")
        if command == 'start':
            draft_counter.start()
        elif command == 'stop':
            draft_counter.stop()
        elif command == 'cancel':
            draft_counter.cancel()
        elif command == 'reset':
            counters.reset_all()
        elif command == 'fixtures':
            initial = floor(time()) - 3600 * 24 * 365 * 3
            max = floor(time())
            while initial < max:
                duration = random.randint(3600, 3600 * 8)
                draft_counter.create(initial, initial + duration)
                pause_duration = random.randint(15 * 60, 3 * 24 * 3600)
                initial += duration + pause_duration
        else:
            run = False
    print("stopped")
    del draft_counter
