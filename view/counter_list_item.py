from datetime import datetime
from math import floor

from PySide6.QtCore import Slot
from PySide6.QtWidgets import QWidget, QHBoxLayout, QLabel, QPushButton, QLineEdit

from model.counter import CounterModel


class CounterListItemWidget(QWidget):

    def __init__(self, counter: CounterModel):
        super().__init__()
        self.counter = counter
        counter_widget_layout = QHBoxLayout(self)
        counter_widget_layout.setContentsMargins(0, 0, 0, 0)
        self.start_label = QLineEdit()
        self.start_label.setFixedWidth(150)
        self.end_label = QLineEdit()
        self.end_label.setFixedWidth(150)
        self.total_label = QLabel()
        self.total_label.setFixedWidth(150)
        edit_button = QPushButton('Save')
        # noinspection PyUnresolvedReferences
        edit_button.clicked.connect(self.action_edit_counter)
        delete_button = QPushButton('Delete')
        # noinspection PyUnresolvedReferences
        delete_button.clicked.connect(self.action_remove_counter)
        counter_widget_layout.addWidget(self.start_label)
        counter_widget_layout.addWidget(self.end_label)
        counter_widget_layout.addWidget(self.total_label)
        counter_widget_layout.addStretch()
        counter_widget_layout.addWidget(edit_button)
        counter_widget_layout.addWidget(delete_button)
        counter.updated.connect(self.update_counter_data)
        self.update_counter_data(counter)

    @Slot(CounterModel)
    def update_counter_data(self, counter: CounterModel):
        start_date = datetime.fromtimestamp(counter.start_at)
        end_date = datetime.fromtimestamp(counter.end_at)
        self.start_label.setText(start_date.strftime('%Y-%m-%d %H:%M:%S'))
        self.end_label.setText(end_date.strftime('%Y-%m-%d %H:%M:%S'))
        self.total_label.setText('Duration: ' + str(end_date - start_date))

    @Slot()
    def action_remove_counter(self):
        self.counter.delete()

    @Slot()
    def action_edit_counter(self):
        self.counter.start_at = floor(datetime.fromisoformat(self.start_label.text().strip()).timestamp())
        self.counter.end_at = floor(datetime.fromisoformat(self.end_label.text().strip()).timestamp())
        self.counter.update()
