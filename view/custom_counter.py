from datetime import datetime
from math import floor

from PySide6.QtWidgets import QWidget, QHBoxLayout, QLineEdit, QPushButton

from model.counter_draft import DraftCounter


class CustomCounterWidget(QWidget):

    def __init__(self, draft_counter: DraftCounter):
        super().__init__()
        self.draft_counter = draft_counter
        counter_widget_layout = QHBoxLayout(self)
        counter_widget_layout.setContentsMargins(0, 0, 0, 0)
        self.start_label = QLineEdit()
        self.start_label.setText(datetime.now().strftime('%Y-%m-%d %H:%M:%S'))
        self.start_label.setFixedWidth(150)
        self.end_label = QLineEdit()
        self.end_label.setFixedWidth(150)
        self.end_label.setText(datetime.now().strftime('%Y-%m-%d %H:%M:%S'))
        save_button = QPushButton('Save')
        # noinspection PyUnresolvedReferences
        save_button.clicked.connect(self.action_save_counter)

        counter_widget_layout.addWidget(self.start_label)
        counter_widget_layout.addWidget(self.end_label)
        counter_widget_layout.addStretch()
        counter_widget_layout.addWidget(save_button)

    def action_save_counter(self):
        start_at = floor(datetime.fromisoformat(self.start_label.text().strip()).timestamp())
        end_at = floor(datetime.fromisoformat(self.end_label.text().strip()).timestamp())
        self.draft_counter.create(start_at, end_at)
        self.start_label.setText(datetime.now().strftime('%Y-%m-%d %H:%M:%S'))
        self.end_label.setText(datetime.now().strftime('%Y-%m-%d %H:%M:%S'))
