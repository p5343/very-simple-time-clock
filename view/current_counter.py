from datetime import datetime, timedelta
from math import floor
from time import time as now

from PySide6.QtCore import Slot, QTimer
from PySide6.QtWidgets import QHBoxLayout, QLabel, QPushButton, QWidget

from model.counter_draft import DraftCounter


class CurrentCounterWidget(QWidget):

    def __init__(self, draft_counter: DraftCounter):
        super().__init__()
        self.draft_counter = draft_counter

        self.current_time_label = QLabel()
        self.current_time_label.setFixedWidth(150)
        self.total_time_label = QLabel()
        self.total_time_label.setFixedWidth(150)
        self.total_time_today_label = QLabel()
        self.total_time_today_label.setFixedWidth(150)
        self.button_start_stop = QPushButton()
        # noinspection PyUnresolvedReferences
        self.button_start_stop.clicked.connect(self.action_start_stop)
        self.button_cancel = QPushButton("Cancel")
        # noinspection PyUnresolvedReferences
        self.button_cancel.clicked.connect(self.action_cancel)
        self.build_view()
        self.update_counter_data(self.draft_counter.start_at, self.draft_counter.end_at,
                                 self.draft_counter.currently_running)
        draft_counter.updated.connect(self.update_counter_data)
        timer = QTimer(self)
        # noinspection PyUnresolvedReferences
        timer.timeout.connect(self.update_refresh)
        timer.start(1000)

    def build_view(self):
        current_counter_layout = QHBoxLayout(self)
        current_counter_layout.setContentsMargins(0, 0, 0, 0)
        current_counter_layout.addWidget(self.current_time_label)
        current_counter_layout.addWidget(self.total_time_label)
        current_counter_layout.addWidget(self.total_time_today_label)
        current_counter_layout.addStretch()
        current_counter_layout.addWidget(self.button_start_stop)
        current_counter_layout.addWidget(self.button_cancel)

    @Slot()
    def update_refresh(self):
        today_total = timedelta()
        if self.draft_counter.currently_running:
            start_date = datetime.fromtimestamp(self.draft_counter.start_at)
            end_date = datetime.fromtimestamp(floor(now()))
            today_total += end_date - start_date
            self.total_time_label.setText('Duration: ' + str(end_date - start_date))
        else:
            self.current_time_label.setText(datetime.now().strftime('%Y-%m-%d %H:%M:%S'))
        today_total += timedelta(0, self.draft_counter.current_group.total_seconds())
        self.total_time_today_label.setText('Today: ' + str(today_total))

    # noinspection PyUnusedLocal
    @Slot(int, int, bool)
    def update_counter_data(self, start_at: int, end_at: int, currently_running: bool):
        if currently_running:
            start_date = datetime.fromtimestamp(start_at)
            end_date = datetime.fromtimestamp(floor(now()))
            self.current_time_label.setText(
                start_date.strftime('%Y-%m-%d %H:%M:%S')
            )
            self.button_start_stop.setText("Stop")
            self.button_cancel.show()
            self.total_time_label.setText('Duration: ' + str(end_date - start_date))
        else:
            self.current_time_label.setText(datetime.now().strftime('%Y-%m-%d %H:%M:%S'))
            self.button_start_stop.setText("Start")
            self.button_cancel.hide()
            self.total_time_label.setText('')

    @Slot()
    def action_start_stop(self):
        if self.draft_counter.currently_running:
            self.draft_counter.stop()
        else:
            self.draft_counter.start()

    @Slot()
    def action_cancel(self):
        if self.draft_counter.currently_running:
            self.draft_counter.cancel()
