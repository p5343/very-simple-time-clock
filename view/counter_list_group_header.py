from datetime import datetime, timedelta

from PySide6.QtCore import Slot
from PySide6.QtWidgets import QWidget, QHBoxLayout, QLabel

from model.counter_group import CounterGroupModel


class CounterListGroupItemWidget(QWidget):

    def __init__(self, counter: CounterGroupModel):
        super().__init__()
        self.counter = counter
        counter_widget_layout = QHBoxLayout(self)
        counter_widget_layout.setContentsMargins(0, 0, 0, 0)
        self.start_label = QLabel()
        self.start_label.setFixedWidth(150)
        self.total_label = QLabel()
        self.total_label.setFixedWidth(150)
        counter_widget_layout.addWidget(self.start_label)
        counter_widget_layout.addWidget(self.total_label)
        counter_widget_layout.addStretch()

        counter.updated.connect(self.update_counter_data)
        self.update_counter_data(counter.start_at, counter.total_seconds())

    @Slot(int, int)
    def update_counter_data(self, start_at: int, total_seconds: int):
        start_date = datetime.fromtimestamp(start_at)
        self.start_label.setText(start_date.strftime('%Y-%m-%d %H:%M:%S'))
        self.total_label.setText('Duration: ' + str(timedelta(0, total_seconds)))
