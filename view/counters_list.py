from typing import Union

from PySide6.QtCore import Slot
from PySide6.QtWidgets import QWidget, QVBoxLayout, QScrollArea

from model.counter import CounterModel
from model.counter_group import CounterGroupModel
from model.counters_list import CountersListModel
from view.counter_list_group_header import CounterListGroupItemWidget
from view.counter_list_item import CounterListItemWidget


class CountersListWidget(QScrollArea):

    def __init__(self, list_model: CountersListModel):
        super().__init__()
        self.manager = list_model
        self.setWidgetResizable(True)
        container_widget = QWidget()
        self.setWidget(container_widget)
        self.layout = QVBoxLayout(container_widget)
        self.layout.setDirection(QVBoxLayout.BottomToTop)

        self.build_view()

        list_model.appended.connect(self.update_add_counter)
        list_model.removed.connect(self.update_remove_counter)

    def build_view(self):
        for i in range(0, len(self.manager.counters)):
            counter = self.manager.counters[i]
            self.update_add_counter(counter)

    @Slot(CounterModel)
    @Slot(CounterGroupModel)
    def update_add_counter(self, counter: Union[CounterModel, CounterGroupModel]):
        if isinstance(counter, CounterModel):
            counter_widget = CounterListItemWidget(counter)
            self.layout.addWidget(counter_widget)
        else:
            counter_widget = CounterListGroupItemWidget(counter)
            self.layout.addWidget(counter_widget)

    @Slot(int)
    def update_remove_counter(self, index: int):
        list_item_widget = self.layout.itemAt(index).widget()
        # noinspection PyTypeChecker
        list_item_widget.setParent(None)
        self.layout.removeWidget(list_item_widget)

    @Slot()
    def update_reset_all(self):
        for i in range(self.layout.count() - 1, -1, -1):
            list_item_widget = self.layout.itemAt(i).widget()
            # noinspection PyTypeChecker
            list_item_widget.setParent(None)
            self.layout.removeWidget(list_item_widget)
