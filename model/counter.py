from uuid import uuid4

from PySide6.QtCore import QObject, Signal


class CounterModel(QObject):
    deleted = Signal(object)
    updated = Signal(object)

    def __init__(self, counter_uuid: str, start_at: int, end_at: int):
        super().__init__()
        self.uuid = counter_uuid
        self.start_at = start_at
        self.end_at = end_at

    def __eq__(self, other):
        return self.uuid == other.uuid

    def __lt__(self, other):
        return self.start_at < other.start_at

    def __gt__(self, other):
        return self.start_at > other.start_at

    def __str__(self):
        return '[counter:' + self.uuid + '] ' + str(self.start_at) + ' -> ' + str(self.end_at)

    ##########
    # [C]RUD
    ##########

    @staticmethod
    def create(startAt: int, endAt: int):
        uuid = uuid4()
        with open('counters', 'a') as list_io:
            list_io.write(str(uuid) + ' ' + str(startAt) + ' ' + str(endAt) + '\n')
        return CounterModel(str(uuid), startAt, endAt)

    ##########
    # C[R]UD
    ##########

    ##########
    # CR[U]D
    ##########

    def update(self):
        with open('counters', 'r+') as list_io:
            lines = list_io.readlines()
            list_io.seek(0)
            list_io.truncate()
            for line in lines:
                uuid = line.strip().split(' ', 1)[0]
                if uuid != self.uuid:
                    list_io.write(line)
                else:
                    list_io.write(str(self.uuid) + ' ' + str(self.start_at) + ' ' + str(self.end_at) + '\n')
        # noinspection PyUnresolvedReferences
        self.updated.emit(self)

    ##########
    # CRU[D]
    ##########

    def delete(self):
        with open('counters', 'r+') as list_io:
            lines = list_io.readlines()
            list_io.seek(0)
            list_io.truncate()
            for line in lines:
                uuid = line.strip().split(' ', 1)[0]
                if uuid != self.uuid:
                    list_io.write(line)
        # noinspection PyUnresolvedReferences
        self.deleted.emit(self)
