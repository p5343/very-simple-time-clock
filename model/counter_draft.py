import os
from math import floor
from pathlib import Path
from time import time as now

from PySide6.QtCore import QObject, Signal

from model.counter import CounterModel
from model.counter_group import CounterGroupModel


class DraftCounter(QObject):
    created = Signal(CounterModel)
    updated = Signal(int, int, bool)

    def __init__(self):
        super().__init__()
        self.start_at = 0
        self.end_at = 0
        if Path('current').exists():
            with open('current', 'r+') as current_io:
                line = current_io.readline()
                self.start_at = int(line.strip()) if line else 0
        i = 3600 * 24
        self.today_start = floor(now() / i) * i
        self.today_end = self.today_start + i
        self.current_group = CounterGroupModel(self.today_start)
        self.currently_running = (self.start_at > 0 and self.end_at == 0)

    def start(self):
        start_at = floor(now())
        filename = Path('current')
        if not (filename.exists()):
            filename.touch()
        with open('current', 'r+') as currentIo:
            currentIo.write(str(start_at))
        self.start_at = start_at
        self.currently_running = True
        # noinspection PyUnresolvedReferences
        self.updated.emit(self.start_at, self.end_at, self.currently_running)

    def create(self, start_at: int, end_at: int):
        new_counter = CounterModel.create(start_at, end_at)
        # noinspection PyUnresolvedReferences
        self.created.emit(new_counter)
        if self.today_start <= new_counter.start_at < self.today_end:
            self.current_group.add(new_counter)

    def reset_values(self):
        self.start_at = 0
        self.end_at = 0
        self.currently_running = False
        if Path('current').exists():
            os.remove('current')
        # noinspection PyUnresolvedReferences
        self.updated.emit(self.start_at, self.end_at, self.currently_running)

    def stop(self):
        if not self.currently_running:
            return False
        new_counter = CounterModel.create(self.start_at, floor(now()))
        # noinspection PyUnresolvedReferences
        self.created.emit(new_counter)
        if self.today_start <= new_counter.start_at < self.today_end:
            self.current_group.add(new_counter)
        self.reset_values()
        return True

    def cancel(self):
        if not self.currently_running:
            return False
        self.reset_values()
        return True
