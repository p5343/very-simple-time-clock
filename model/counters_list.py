from math import floor
from pathlib import Path

from PySide6.QtCore import QObject, Signal, Slot

from model.counter import CounterModel
from model.counter_draft import DraftCounter
from model.counter_group import CounterGroupModel


class CountersListModel(QObject):
    removed = Signal(int)
    appended = Signal(CounterModel)

    def __init__(self, draft_counter: DraftCounter, organize_by: str):
        super().__init__()
        self.counters = []
        filename = Path('counters')
        if not (filename.exists()):
            filename.touch()
        with open('counters', 'r') as list_io:
            for line in list_io:
                values = line.strip().split(" ")
                model = CounterModel(values[0], int(values[1]), int(values[2]))
                model.deleted.connect(self.delete)
                self.counters.append(model)
        self.organize(organize_by)
        self.add_today_counters_to_current(draft_counter)
        draft_counter.created.connect(self.append)

    def __str__(self):
        result = ''
        for counter in self.counters:
            result += '*** ' + str(counter) + '\n'
        return result

    def add_today_counters_to_current(self, draft_counter: DraftCounter):
        for i in range(len(self.counters) - 1, -1, -1):
            group = self.counters[i]
            if isinstance(group, CounterGroupModel):
                if group.start_at == draft_counter.current_group.start_at:
                    for counter in group.counters:
                        draft_counter.current_group.add(counter)
                elif group.start_at < draft_counter.current_group.start_at:
                    break

    def organize(self, interval: str):
        self.counters.sort()
        # S'il n'y a pas de compteurs, il n'y a rien à organiser
        if len(self.counters) == 0:
            return True
        # Définition d'un interval de regroupement
        i = 0
        if interval == 'day':
            i = 3600 * 24
        # Si aucun interval de regroupement, il n'y a pas besoin d'organiser les compteurs
        if i == 0:
            return True

        # Les commentaires suivants sont basés sur un interval d'1 jour.
        # Le concept est identique pour tout autre interval
        new_counters_list = []

        # Je démarre le premier groupe (au jour du premier compteur)
        start_at = floor(self.counters[0].start_at / i) * i
        current_group = CounterGroupModel(start_at)

        for counter in self.counters:
            # On est toujours sur le même jour ⇒ on ajoute le temps de ce compteur au total de ce jour
            if counter.start_at < start_at + i:
                current_group.add(counter)
                new_counters_list.append(counter)

            # On est passé au jour suivant donc j'ajoute le total de ce jour avant d'avancer
            if counter.start_at >= start_at + i:
                # Le groupe précédent est terminé, je l'insère dans la liste
                new_counters_list.append(current_group)
                # Je crée un nouveau groupe
                start_at = floor(counter.start_at / i) * i
                current_group = CounterGroupModel(start_at)
                # J'ajoute le compteur dans le groupe suivant
                current_group.add(counter)
                new_counters_list.append(counter)

        # Le dernier groupe est terminé, je l'insère dans la liste (on n'est jamais 'passé au jour suivant', indeed)
        new_counters_list.append(current_group)

        self.counters = new_counters_list

    @Slot(CounterModel)
    def append(self, counter: CounterModel):
        self.counters.append(counter)
        counter.deleted.connect(self.delete)
        # noinspection PyUnresolvedReferences
        self.appended.emit(counter)

    @Slot(CounterModel)
    def delete(self, counter: CounterModel):
        # noinspection PyUnresolvedReferences
        self.removed.emit(self.counters.index(counter))
        self.counters.remove(counter)

    def reset_all(self):
        if len(self.counters) > 0:
            counter = self.counters[len(self.counters) - 1]
            counter.delete()
            self.reset_all()
