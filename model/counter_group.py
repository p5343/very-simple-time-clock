from PySide6.QtCore import QObject, Signal, Slot

from model.counter import CounterModel


class CounterGroupModel(QObject):
    updated = Signal(int, int)

    def __init__(self, start_at: int):
        super().__init__()
        self.uuid = str(start_at)
        self.start_at = start_at
        self.counters = []

    def total_seconds(self) -> int:
        total = 0
        for counter in self.counters:
            total += counter.end_at - counter.start_at
        return total

    def add(self, counter: CounterModel):
        counter.updated.connect(self.update_counter)
        counter.deleted.connect(self.update_delete_counter)
        self.counters.append(counter)
        # noinspection PyUnresolvedReferences
        self.updated.emit(self.start_at, self.total_seconds())

    @Slot(CounterModel)
    def update_counter(self):
        # noinspection PyUnresolvedReferences
        self.updated.emit(self.start_at, self.total_seconds())

    @Slot(CounterModel)
    def update_delete_counter(self, counter: CounterModel):
        self.counters.remove(counter)
        # noinspection PyUnresolvedReferences
        self.updated.emit(self.start_at, self.total_seconds())
