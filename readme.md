# Installation

```
git clone git@gitlab.com:p5343/very-simple-time-clock.git
python -m venv env
source env/bin/activate
pip install pyside6
```

# Usage

```
source env/bin/activate
python3 ui.py
```

# User Stories

(En tant qu'utilisateur de l'application)
- [x] Je peux voir les compteurs (terminés, avec une date de début et une date de fin - voir classes CounterModel et CountersListModel)
- [x] Je peux démarrer un compteur (enregistrement date de début dans un fichier temporaire - voir classe CounterDraftModel)
- [x] Je peux arrêter un compteur en cours (enregistrement - enregistrement dans fichier final - voir classe CounterModel)
- [x] Je peux annuler un compteur en cours (annulation - suppression du fichier temporaire)
- [x] Je peux voir si je suis en train d'enregistrer le temps, et si oui depuis quelle heure (compteur démarré, pas encore terminé)
- [-] Je peux supprimer tous les compteurs
  - [ ] BUG : s'il y a trop de compteurs on tombe en `maximum recursion depth`
- [x] Je peux créer un compteur personnalisé
  - [x] En ligne de commande
  - [x] Via l'interface graphique
- [x] Je peux modifier un compteur existant
- [x] Je peux choisir de terminer le compteur en route avant de quitter l'application
- [x] Pendant qu'un compteur est en route, je peux voir à quelle heure il a démarré et le temps total
- [x] Je peux voir les compteurs organisés par journées
  - [ ] Et j'aimerais pouvoir choisir ma vue préférée, actuellement, c'est seulement paramétrable dans le code
- [ ] Je peux afficher / démarrer / arrêter mon compteur via le menu d'état
- [ ] Je peux voir les compteurs organisés par semaines
- [ ] Je peux voir les compteurs organisés par mois
- [ ] Je peux voir les compteurs organisés par années

# Code Architecture

## Modèle-Vue

- Méthode `update_...` = La vue se met à jour suite à une modification du modèle
- Méthode `action_...` = La vue demande au modèle de se mettre à jour suite à une action de l'utilisateur
- [ ] Faire un diagramme